package SE_Examination.EX1;

interface  U{}

public class I implements U {
    private long t;

    private K k;

    public void f(){}
    public void i(J j){}
}
class J{}
class N{
    I i;
}
class K{
    private L l;
    private S s;

    public K(){
        l = new L();
    }
    public void function(){
        s = new S();
    }
}
class L{
    public void metA(){}
}
class S{
    public void metB(){}
}

