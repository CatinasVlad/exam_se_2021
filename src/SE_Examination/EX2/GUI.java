package SE_Examination.EX2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

    JTextField jTextField1;
    JTextField jTextField2;
    JTextField jTextField3;
    JButton jButton;

    GUI(){
        setTitle("Summing of two integers!");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        init();
        setSize(300,300);
        setVisible(true);
    }

    public void init(){
        setLayout(null);
        jTextField1 = new JTextField();
        jTextField1.setBounds(10,20,200,20);

        jTextField2 = new JTextField();
        jTextField2.setBounds(10,50,200,20);

        jTextField3 = new JTextField();
        jTextField3.setBounds(10,80,200,20);

        jButton = new JButton("Click me!");
        jButton.setBounds(10,200,200,20);
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String str1 = jTextField1.getText();
                String str2 = jTextField2.getText();

                int var1 = Integer.parseInt(str1);
                int var2 = Integer.parseInt(str2);

                int sum = var1 + var2;

                String result = String.valueOf(sum);

                jTextField3.setText(result);

            }
        });

        add(jTextField1);
        add(jTextField2);
        add(jTextField3);
        add(jButton);
    }

    public static void main(String[] args) {
        new GUI();
    }
}
